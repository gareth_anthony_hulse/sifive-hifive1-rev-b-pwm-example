/* © Copyright 2020 Gareth Anthony Hulse
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

namespace app
{
  class prci
  {
  private:
    const int m_base = 0x10008000;
      
      
    volatile int
    *m_hfrosc_cfg = reinterpret_cast<int*> (m_base),
      *m_hfxosc_cfg = reinterpret_cast<int*> (m_base + 0x4),
      *m_pll_cfg = reinterpret_cast<int*> (m_base + 0x8),
      *m_pll_out_div = reinterpret_cast<int*> (m_base + 0xC),
      *m_proc_mon_cfg = reinterpret_cast<int*> (m_base + 0xF0),

      *m_interrupt_enable2 = reinterpret_cast<int*> (0x0C002004);

  public:
    struct bitmask
    {
      struct hfrosc_cfg
      {
	static const int
	enable = 1 << 30,
	  disable = 0 << 30,
	  ready = 1 << 31;
      };

      struct pll_cfg
      {
	static const int
	select = 1 << 16,
	  reference_select = 1 << 17,
	  bypass = 1 << 18;
      };
    };
      
    void
    hfrosc_cfg (const int data)
    {
      *m_hfrosc_cfg = data;
    }

    void
    pll_cfg (const int data)
    {
      *m_pll_cfg = data;
    }

    void
    interrupt_enable2 (const int data)
    {
      *m_interrupt_enable2 = data;
    }
  };

}

